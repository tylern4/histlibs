from setuptools import setup
import subprocess

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

git_version = subprocess.check_output(
    ['git', 'rev-parse', '--short', 'HEAD']).decode("utf-8")[:-1]

with open("HistLibs/__version__", "r", encoding="utf-8") as fh:
    version = fh.read()
    version = version + "." + git_version


setup(
    name='HistLibs',
    version=version,
    description='A example Python package',
    url='https://gitlab.com/tylern4/histlibs',
    author='Nick Tyler',
    author_email='nicholas.s.tyler.4@gmail.com',
    license='BSD 2-clause',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['HistLibs'],
    install_requires=['matplotlib',
                      'numpy',
                      'boost-histogram',
                      'scipy',
                      'lmfit'
                      ],

    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    python_requires='>=3.5',
)
